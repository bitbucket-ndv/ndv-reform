import gulp from 'gulp';
import runSequence from 'run-sequence';

gulp.task('default', function() {
	return runSequence(
		'copy',
		'styles',
		'jade',
		'webpack',
		'browserSync',
		'watch'
		);
});

gulp.task('build', () => {
	runSequence(
		'del',
		'copy',
		'spritesmith',
		'styles',
		'jade',
		'webpack'
	);
});
