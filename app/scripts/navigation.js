import {transitionShow, transitionHide} from './utils';

let isOpen = false;

const navigation = {
	initialize: function() {
		const navButton = document.querySelector('.secondary-nav-button');
		navButton.addEventListener('click', navigation.toggle);
	},

	toggle: function() {
		const nav = document.querySelector('.secondary-nav');
		if (isOpen) {
			$('.secondary-nav-button').removeClass('is-active');
			transitionHide(nav, 'is-open', 'is-animating');
		} else {
			$('.secondary-nav-button').addClass('is-active');
			transitionShow(nav, 'is-open', 'is-animating');
		}
		isOpen = !isOpen;
	}
};

export default navigation;
