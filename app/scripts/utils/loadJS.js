export default function loadJS(src) {
	return new Promise((resolve, reject) => {
		const ref = window.document.getElementsByTagName('script')[0];
		const script = window.document.createElement('script');
		script.src = src;
		script.async = true;
		ref.parentNode.insertBefore(script, ref);
		script.onload = resolve;
		script.onerror = reject;
	});
}
