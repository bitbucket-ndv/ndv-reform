function transitionToClass({removeClass = false} = {}) {
	return function(el, activeClass = 'active', transitionClass = 'transition') {
		return new Promise(resolve => {
			if (!removeClass) {
				requestAnimationFrame(() => {
					el.classList.add(transitionClass);
				});
			}

			el.addEventListener('transitionend', function listener() {
				resolve();
				if (removeClass) {
					el.classList.remove(activeClass);
				}
				el.removeEventListener('transitionend', listener);
			});

			el.classList[removeClass ? 'remove' : 'add'](removeClass ? transitionClass : activeClass);
		});
	};
}

export const transitionShow = transitionToClass();
export const transitionHide = transitionToClass({removeClass: true});
