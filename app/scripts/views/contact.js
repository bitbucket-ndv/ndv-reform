/* globals google, $ */
import scrollbar from 'perfect-scrollbar';
import loadJS from '../utils/loadJS';
import googleStyles from '../map/googleStyles';
import {getMarkerOpts} from '../map/googleMarker';
import {getInfoContent} from '../map/googleTemplate';

let map;

const view = {
	onMapLoad: function() {
		map = new google.maps.Map(document.getElementById('contact-map'), {
			center: {lat: 55.800365, lng: 37.719730},
			zoom: 15,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: false
		});

		map.setOptions({styles: googleStyles});

		require.ensure('../map/points', () => {
			const points = require('../map/points');
			points.objects.forEach(data => view.setMarker('object', data));
			points.offices.forEach(data => view.setMarker('office', data));
		}, 'points');

		require.ensure('../map/routes', () => {
			const routes = require('../map/routes');
			view.routes = [];
			routes.forEach(routeData => {
				const route = new google.maps.Polyline({
					strokeColor: routeData.strokeColor,
					strokeOpacity: 1.0,
					strokeWeight: 5,
					path: routeData.coords
				});
				route.setMap(map);
				view.routes.push(route);
			});
		}, 'routes');
	},

	activateOffice: function(index) {
		const {infoWindow, marker} = view.officePoints[index];
		infoWindow.open(map, marker);
		map.setCenter(marker.getPosition());
		map.panBy(200, -100);
	},

	setMarker: function(type, data) {
		const marker = new google.maps.Marker(
			Object.assign(
				{},
				{
					position: {lat: data.coords[0], lng: data.coords[1]},
					title: data.properties.title || '',
					map: map
				},
				getMarkerOpts(type)
			)
		);
		const infoWindow = new google.maps.InfoWindow({
			content: getInfoContent(data.properties)
		});
		infoWindow.addListener('domready', function() {
			const iwOuter = $('.gm-style-iw');
			const iwBackground = iwOuter.prev();

			iwBackground.children(':nth-child(1)').css({'display': 'none'});
			iwBackground.children(':nth-child(2)').css({'display': 'none'});
			iwBackground.children(':nth-child(3)').addClass('balloon-tip');
			iwBackground.children(':nth-child(4)').css({'display': 'none'});
		});
		if (!view.officePoints) {
			view.officePoints = [];
		}
		if (type === 'office') {
			view.officePoints.push({infoWindow, marker});
		}

		marker.addListener('click', () => {
			infoWindow.open(map, marker);
		});
	},

	initSelector: function() {
		const $selector = $('.office-selector');
		let isOpen = false;

		function toggle() {
			isOpen = !isOpen;
			$selector.toggleClass('is-open');
		}
		$selector.find('.selector__label').on('click', toggle);
		$selector.on('click', '.selector__item', function() {
			const label = $(this).text();
			$selector.find('.selector__label').text(label);
			toggle();
			view.activateOffice($(this).data('index'));
		});

		require.ensure('../map/points', () => {
			const points = require('../map/points');
			points.offices.forEach((office, i) => {
				if (!office.properties.metro) {
					return;
				}
				$selector
						.find('.selector__dropdown')
						.append($('<div class="selector__item">' + office.properties.metro + '</div>').data('index', i));
			});
		}, 'points');
	}
};

export default function() {
	$('.scrollbar').each(function() {
		scrollbar.initialize(this, {suppressScrollY: true});
	});

	view.initSelector();

	window.cornerMapContact = view.onMapLoad;
	loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyCwsCTj2EoKgZv80xYhVIU6vD5508kydqY&callback=cornerMapContact');

	$('.page-viewport').addClass('wh-shadow');
}
