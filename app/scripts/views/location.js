/* globals google, $ */
import scrollbar from 'perfect-scrollbar';
import loadJS from '../utils/loadJS';
import googleStyles from '../map/googleStyles';
import {getMarkerOpts} from '../map/googleMarker';
import {getInfoContent} from '../map/googleTemplate';

let map;
let label;
let routeVisible = true;
let infrastructureVisible = true;

const view = {
	routes: [],
	infrastructure: [],
	onMapLoad: function() {
		map = new google.maps.Map(document.getElementById('location-map'), {
			center: {lat: 55.800365, lng: 37.724430},
			zoom: 16,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: false
		});

		map.setOptions({styles: googleStyles});

		require.ensure('../map/points', () => {
			const points = require('../map/points');
			points.objects.forEach(data => view.setMarker('object', data));
			points.offices.forEach(data => view.setMarker('office', data));
		}, 'points');

		require.ensure('../map/infrastructure', () => {
			const points = require('../map/infrastructure');
			points.forEach(data => view.setIcon(data));
		}, 'infrastructure');

		require.ensure('../map/routes', () => {
			const routes = require('../map/routes');
			routes.forEach(routeData => {
				const route = new google.maps.Polyline({
					strokeColor: routeData.strokeColor,
					strokeOpacity: 1.0,
					strokeWeight: 5,
					path: routeData.coords
				});
				route.setMap(map);
				route.setOptions({visible: true});
				view.routes.push(route);
			});
		}, 'routes');

		$('.block-1').on('click', function() {
			infrastructureVisible = !infrastructureVisible;
			view.infrastructure.forEach(r => r.setOptions({visible: infrastructureVisible}));
		});
		$('.block-2').on('click', function() {
			routeVisible = !routeVisible;
			view.routes.forEach(r => r.setOptions({visible: routeVisible}));
		});
	},

	setMarker: function(type, data) {
		const marker = new google.maps.Marker(
			Object.assign(
				{},
				{
					position: {lat: data.coords[0], lng: data.coords[1]},
					title: data.properties.title || '',
					map: map
				},
				getMarkerOpts(type)
			)
		);
		const infoWindow = new google.maps.InfoWindow({
			content: getInfoContent(data.properties)
		});
		infoWindow.addListener('domready', function() {
			const iwOuter = $('.gm-style-iw');
			const iwBackground = iwOuter.prev();

			iwBackground.children(':nth-child(1)').css({'display': 'none'});
			iwBackground.children(':nth-child(2)').css({'display': 'none'});
			iwBackground.children(':nth-child(3)').addClass('balloon-tip');
			iwBackground.children(':nth-child(4)').css({'display': 'none'});
		});
		if (!view.officePoints) {
			view.officePoints = [];
		}
		if (type === 'office') {
			view.officePoints.push({infoWindow, marker});
		}

		marker.addListener('click', () => {
			infoWindow.open(map, marker);
		});
	},

	setIcon(data) {
		const title = data.properties && data.properties.title || '';
		const marker = new google.maps.Marker(
			{
				position: {lat: data.coords[0], lng: data.coords[1]},
				map: map,
				icon: {
					url: data.icon.src,
					size: new google.maps.Size(data.icon.w, data.icon.h),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(0, data.icon.h)
				}
			}
		);
		marker.addListener('mouseover', function(event) {
			const topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
			const bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
			const scale = Math.pow(2, map.getZoom());
			const worldPoint = map.getProjection().fromLatLngToPoint(event.latLng);
			const x = (worldPoint.x - bottomLeft.x) * scale;
			const y = (worldPoint.y - topRight.y) * scale;
			if (title) {
				label
					.css({left: x, top: y})
					.text(title)
					.show();
				label.appendTo(document.body);
			}
		});
		marker.addListener('mouseout', () => label.hide());
		view.infrastructure.push(marker);
	},
};

export default function() {
	$('.block-1, .block-2').on('click', function() {
		const $elm = $(this);
		let forLabel;
		if (!$elm.hasClass('active')) {
			forLabel = $elm.attr('data-for');
			$elm
				.addClass('active')
				.siblings()
				.removeClass('active');
		} else {
			$elm.removeClass('active');
			forLabel = 'default';
		}

		$(document)
			.find('.text-content .text')
			.removeClass('active')
			.filter(`[data-label="${forLabel}"]`)
			.addClass('active');

		scrollbar.update(document.querySelector('.scrollbar'));
	});

	$('.page-viewport').addClass('wh-shadow');

	label = $('<div class="map-label" />');
	window.cornerMapContact = view.onMapLoad;
	loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyCwsCTj2EoKgZv80xYhVIU6vD5508kydqY&callback=cornerMapContact');
}
