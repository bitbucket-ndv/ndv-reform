import scrollbar from 'perfect-scrollbar';

export default function() {
	$('.scrollbar').each(function() {
		scrollbar.initialize(this, {suppressScrollX: true, maxScrollbarLength: 15});
	});
}
