/* globals $ */
import '../libs/fancybox';

const slickOptions = {
	default: {
		dots: false,
		draggable: false,
		autoplay: false,
		arrows: false,
		infinite: false
	},
	media: [
		{val: '(max-width: 1400px)', opts: {slidesToShow: 3}},
		{val: '(min-width: 1400px)', opts: {rows: 2, slidesToShow: 4}}
	]
};

const view = {
	onResize: function() {
		slickOptions.media.some(media => {
			const isMatched = window.matchMedia(media.val).matches;
			if (isMatched) {
				$('.slick-slider')
						.slick('unslick')
						.slick(Object.assign(slickOptions.default, media.opts));
			}
			return isMatched;
		});
	}
};

export default function() {
	$('.slick-slider').slick(slickOptions.default);

	$('.control .control__next').on('click', () => {
		$('.slick-slider').slick('slickNext');
	});
	$('.control .control__prev').on('click', () => {
		$('.slick-slider').slick('slickPrev');
	});

	view.onResize();
	$(window).on('resize', view.onResize);
}
