/* globals $ */
import scrollbar from 'perfect-scrollbar';

function getHref(row) {
	const clickHandler = row.getAttribute('onclick');
	const queryString = clickHandler.match(/\'(.*?)\'/);

	return queryString ? `/select-apart${queryString[1]}` : '';
}

const sorting = {
	initialize: () => {
		sorting.data = [];
		sorting.collectData();

		$('.param-table th').on('click', function() {
			const sortDirection = $(this).hasClass('asc') ? -1 : 1;
			const columnNum = $(this).attr('data-sort-column') - 1;
			$(this).toggleClass('asc desc');
			sorting.sortColumn(columnNum, sortDirection);
			sorting.render();
		});
	},

	collectData: function() {
		const table = document.querySelector('.sort-table');

		for (let i = 0; i < table.tBodies.length; i++) {
			for (let j = 0; j < table.tBodies[i].rows.length; j++) {
				sorting.data.push({
					hidden: false,
					href: getHref(table.tBodies[i].rows[j]),
					cells: Array.prototype.slice.call(table.tBodies[i].rows[j].cells).map((cell) => parseInt(cell.textContent.replace(/\s+/g, '').replace(',', '.'), 10) || 0)
				});
			}
		}
	},

	sortColumn: function(columnNum, sortDirection) {
		sorting.data.sort((a, b) => sortDirection * (a.cells[columnNum] - b.cells[columnNum]));
	},

	filter: function(columnNum, from, to) {
		sorting.data.forEach(item => {
			item.hidden = (item[columnNum] >= from && item[columnNum] <= to) ? false : true;
		});
	},

	render: function() {
		$('.sort-table tbody').remove();
		const $tbody = $('<tbody />');
		sorting.data.forEach(item => {
			if (item.hidden) {
				return;
			}
			const $tr = $('<tr class="sale" />').data('href', item.href);
			item.cells.forEach((data, cellNum) => {
				let text = data || '';
				if (cellNum >= 6) {
					text = text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
				}
				$tr.append($(`<td>${text}</td>`));
			});
			$tbody.append($tr);
		});
		$('.sort-table').append($tbody);
		scrollbar.update(document.querySelector('.scrollbar'));
	}
};

export default function() {
	sorting.initialize();

	/* eslint-disable */
	var path=location.pathname.split('/');
			$('.housemodbigs').remove();
			$('.kvbesestabsearch').remove();

			var cellFloor = 0;
			var cellRoom = 1;
			var cellCost = 5;

			var Cost = new Array();
			var Rooms = new Array();
			var Floor = new Array();
			$('#realize_table tr:eq(0)').remove();
			$('#realize_table > tbody > tr').each(function () {

			$(this).removeAttr('style').removeAttr('onmouseover').removeAttr('onmouseout');
			var url = $(this).attr('onclick').split('?');
			$(this).attr('onclick',"document.location='/apart-detail/?"+url[1]);
					if ($(this).css({display: ""})) {
							$(this).find('td').each(function (c) {
									if (c == cellFloor) {
											Floor.push($(this).text());
									}
									if (c == cellRoom) {
											Rooms.push($(this).text());
									}
									if (c == cellCost) {
											Cost.push($(this).text().replace(/\s+/g, ''));
											$(this).text($(this).text().split('.')[0])
									}
							});
					}
			});

	if (path[1]=='parking')
	{
		$('.param-section__controls').remove();
		$('.param-table__content').empty().html('<thead>\
											<tr>\
												<th data-sort=\'4\'>Этаж\
												<div class="sort"></div>\
												</th>\
												<th data-sort=\'5\'>Условный \
													<br>номер\
													<div class="sort"></div>\
												</th>\
												<th data-sort=\'6\'>Площадь\
												<br/> м<sup>2</sup>\
													<div class="sort"></div>\
												</th>\
												<th data-sort=\'7\'>Стоимость\
												<br/> руб. \
													<div class="sort"></div>\
												</th>\
											</tr>\
										</thead>');
			$('h1').text('Машиноместа');
	}
	else
	{
		var maxCost = 0; var minCost = 0;
		minCost = Math.min.apply(Math, Cost);
		minCost = Math.floor(minCost / 1000000); // floor -округление в меньшую сторону
		maxCost = Math.max.apply(Math, Cost);
		maxCost = Math.ceil(maxCost / 1000000); // ceil - округление в большую сторону
		var cost_val = [minCost, maxCost];

		var maxRooms = 0;
		var minRooms = Math.min.apply(Math, Rooms);
		var maxRooms = Math.max.apply(Math, Rooms);
		var rooms_val = [minRooms, maxRooms];

		var maxFloor = 0;
		var minFloor = Math.min.apply(Math, Floor);
		maxFloor = Math.max.apply(Math, Floor);
		var floor_val = [minFloor, maxFloor];

		$("#range1").ionRangeSlider({
				type: "double",
			min: floor_val[0],
			max: floor_val[1],
				hide_min_max: true,
				hide_from_to: false,
				grid: false,
				values_separator: ';'
		});
		$("#range2").ionRangeSlider({
				type: "double",
			min: rooms_val[0],
			max: rooms_val[1],
				hide_min_max: true,
				hide_from_to: false,
				grid: false,
				values_separator: ';'
		});
			$("#range3").ionRangeSlider({
					type: "double",
				min: cost_val[0],
				max: cost_val[1],
					hide_min_max: true,
					hide_from_to: false,
					grid: false,
					values_separator: ';'
			});

			$("#range1").on("change", function () {
					getDataTable(cellFloor, cellRoom, cellCost);
			});

			$("#range2").on("change", function () {
					getDataTable(cellFloor, cellRoom, cellCost);
			});

			$("#range3").on("change", function () {
					getDataTable(cellFloor, cellRoom, cellCost);
			});
	}

	function getDataTable(cellFloor, cellRoom, cellCost) {

				var floor_chk = 0;
				var rooms_chk = 0;
				var cost_chk = 0;

				$('#realize_table > tbody> tr').each(function (c) { // each1

						$(this).find('td').each(function (n) { // each2

								if (n == cellFloor) {
										var floorInt = parseInt($(this).text());
										floor_val = $('.js-irs-0 .irs-single').text().split(";");

										if (floor_val[0] <= floorInt && floorInt <= floor_val[1]) {
												floor_chk = 1;
										} else {
												floor_chk = 0;
										}
								}

								if (n == cellRoom) {
										var roomsInt = parseInt($(this).text());
										rooms_val = $('.js-irs-1 .irs-single').text().split(";");
										if (rooms_val[0] <= roomsInt && roomsInt <= rooms_val[1]) {
												rooms_chk = 1;
										} else {
												rooms_chk = 0;
										}
								}

								if (n == cellCost) {
										var costInt = $(this).text();

												costInt = costInt.replace(/\s+/g, '');
												costInt = costInt.split(".");
												costInt = parseInt(costInt[0]);
												var cost_val = $('.js-irs-2 .irs-single').text().split(";");
												var cost_val0 = cost_val[0] * 1000000;
												var cost_val1 = cost_val[1] * 1000000;

												if (cost_val0 <= costInt && costInt <= cost_val1) {
														cost_chk = 1;
												} else {
														cost_chk = 0;
												}
								}

						}); // each2
						if ((floor_chk == 1) && (rooms_chk == 1) && (cost_chk == 1) && (!$(this).hasClass("hide"))) {
								$(this).show();
						}
						else {
								$(this).hide();
						}
				}); // each1
		}
		/* eslint-enable */
}
