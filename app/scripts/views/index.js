/* globals google, $ */
import scrollbar from 'perfect-scrollbar';
import 'slick-carousel';
import loadJS from '../utils/loadJS';
import googleStyles from '../map/googleStyles';
import {transitionShow, transitionHide} from '../utils';
import {getMarkerOpts} from '../map/googleMarker';
import {getInfoContent} from '../map/googleTemplate';

let fullMap;
let isFullMapOpen = false;

const view = {
	activateSliders: function() {
		$('.slider-text').slick({
			dots: true,
			draggable: false,
			autoplay: true,
			slide: '.slider-text__item',
			arrows: false,
			adaptiveHeight: true
		});

		$('.slider-index').slick({
			dots: false,
			draggable: false,
			autoplay: true,
			arrows: false,
			autoplaySpeed: 6000
		});

		$('.slider-index').on('afterChange', function(event, slick, sliderIndex) {
			$('.slider-images .slider-images__image')
						.removeClass('active')
						.eq(sliderIndex)
						.addClass('active');
		});

		$('.slider-images .slider-images__image').on('click', function() {
			const slideIndex = $(this).attr('data-slide');
			$('.slider-index').slick('slickGoTo', slideIndex);
		});

		$('.news-slider').slick({
			dots: true,
			slide: '.news-slider__item',
			autoplay: true,
			arrows: true,
			draggable: false
		});
	},

	handleTextAnimation: function() {
		const elm = document.querySelector('.apart-block');

		elm.addEventListener('animationend', event => {
			if (event.animationName === 'text-animation') {
				elm.classList.add('is-animated');
			}
		});
	},

	toggleFullMap: function() {
		isFullMapOpen = !isFullMapOpen;
		const elm = $('.full-map')[0];
		if (isFullMapOpen) {
			transitionShow(elm, 'is-open', 'is-animating');
		} else {
			transitionHide(elm, 'is-open', 'is-animating');
		}
	},

	setIcon(gMap, data) {
		const marker = new google.maps.Marker(
			{
				position: {lat: data.coords[0], lng: data.coords[1]},
				map: gMap,
				icon: {
					url: data.icon.src,
					size: new google.maps.Size(data.icon.w, data.icon.h),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(0, data.icon.h)
				}
			}
		);
	},

	setMarker: function(gMap, type, data, tooltip) {
		const marker = new google.maps.Marker(
			Object.assign(
				{},
				{
					position: {lat: data.coords[0], lng: data.coords[1]},
					title: data.properties.title || '',
					map: gMap
				},
				getMarkerOpts(type)
			)
		);
		const infoWindow = new google.maps.InfoWindow({
			content: getInfoContent(data.properties)
		});
		infoWindow.addListener('domready', function() {
			const iwOuter = $('.gm-style-iw');
			const iwBackground = iwOuter.prev();

			iwBackground.children(':nth-child(1)').css({'display': 'none'});
			iwBackground.children(':nth-child(2)').css({'display': 'none'});
			iwBackground.children(':nth-child(3)').addClass('balloon-tip');
			iwBackground.children(':nth-child(4)').css({'display': 'none'});
		});
		if (!view.officePoints) {
			view.officePoints = [];
		}
		if (type === 'office') {
			view.officePoints.push({infoWindow, marker});
		}

		if (tooltip) {
			marker.addListener('click', () => {
				infoWindow.open(fullMap, marker);
			});
		}
	},

	onMapLoad: function() {
		const map = new google.maps.Map(document.getElementById('corner-map'), {
			center: {lat: 55.799866, lng: 37.716055},
			zoom: 15,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: false
		});

		map.setOptions({styles: googleStyles});
		map.panBy(-100, -70);

		fullMap = new google.maps.Map(document.getElementById('full-map'), {
			center: {lat: 55.800365, lng: 37.719730},
			zoom: 15,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			zoomControl: false
		});

		fullMap.setOptions({styles: googleStyles});

		require.ensure('../map/points', () => {
			const points = require('../map/points');
			points.objects.forEach(data => {
				view.setMarker(fullMap, 'object', data, true);
				view.setMarker(map, 'object', data);
			});
			view.setIcon(map, {
				coords: [points.objects[0].coords[0] - 0.0015, points.objects[0].coords[1] + 0.002],
				icon: {src: '/pic/design/map-metro.png', w: 35, h: 45}
			});
		}, 'points');

		require.ensure('../map/infrastructure', () => {
			const points = require('../map/infrastructure');
			points.forEach(data => view.setIcon(fullMap, data));
		}, 'infrastructure');

		$('.map-corner, .full-map .close').on('click', event => {
			event.preventDefault();
			view.toggleFullMap();
		});
	}
};

export default function() {
	view.activateSliders();
	view.handleTextAnimation();

	$('.scrollbar').each(function() {
		scrollbar.initialize(this, {suppressScrollY: true});
	});

	window.cornerMapInit = view.onMapLoad;
	loadJS('https://maps.googleapis.com/maps/api/js?key=AIzaSyCwsCTj2EoKgZv80xYhVIU6vD5508kydqY&callback=cornerMapInit');
}
