/* globals $ */

export default function() {
	$('.decor-gallery').slick({
		centerMode: true,
		centerPadding: '60',
		slidesToShow: 3,
		slide: 'img'
	});
}
