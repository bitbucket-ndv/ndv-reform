/* globals $ */
import scrollbar from 'perfect-scrollbar';

export default function() {
	$('.partner').on('click', function() {
		const $elm = $(this);
		const textBlockIndex = $elm.attr('data-for');
		$elm.addClass('active')
				.siblings().removeClass('active');

		$('.partner-description')
			.removeClass('active')
			.filter(`[data-index="${textBlockIndex}"]`)
			.addClass('active');

		scrollbar.update(document.querySelector('.scrollbar'));
	});
}
