/* globals $ */
import 'slick-carousel';

const slickOptions = {
	default: {
		dots: false,
		draggable: false,
		autoplay: false,
		arrows: false,
		infinite: false
	},
	media: [
		{val: '(max-width: 1400px)', opts: {slidesToShow: 2}},
		{val: '(min-width: 1400px)', opts: {rows: 2, slidesToShow: 3}}
	]
};

const view = {
	onResize: function() {
		slickOptions.media.some(media => {
			const isMatched = window.matchMedia(media.val).matches;
			if (isMatched) {
				$('.news-list')
						.slick('unslick')
						.slick(Object.assign(slickOptions.default, media.opts));
			}
			return isMatched;
		});
	}
};

export default function() {
	$('.news-list').slick(slickOptions.default);

	$('.control .control__next').on('click', () => {
		$('.news-list').slick('slickNext');
	});
	$('.control .control__prev').on('click', () => {
		$('.news-list').slick('slickPrev');
	});

	view.onResize();
	$(window).on('resize', view.onResize);
}
