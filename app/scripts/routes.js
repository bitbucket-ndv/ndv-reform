/* globals $ */
import index from './views/index';
import exchange from './views/exchange';
import news from './views/news';
import construction from './views/construction';
import contact from './views/contact';
import param from './views/param';
import location from './views/location';
import partners from './views/partners';
import decor from './views/decor';

export default {
	getView: function() {
		const path = $('.page').attr('data-type');
		switch (path) {
		case 'index':
			return index;
		case 'news':
			return news;
		case 'contact':
			return contact;
		case 'exchange':
			return exchange;
		case 'construction':
			return construction;
		case 'flat-param':
			return param;
		case 'location':
			return location;
		case 'partners':
			return partners;
		case 'decor':
			return decor;
		default:
			return null;
		}
	}
};
