export default [
	 { 'featureType': 'landscape', 'stylers': [ { 'hue': '#7f00ff' }, { 'lightness': -9 }, { 'saturation': 22 }, { 'gamma': 0.58 } ] },
	 { 'featureType': 'road', 'stylers': [ { 'hue': '#8800ff' }, { 'gamma': 0.47 } ] },
	 { 'featureType': 'poi', 'stylers': [ { 'hue': '#8800ff' }, { 'saturation': 17 }, { 'gamma': 0.4 } ] }
];
