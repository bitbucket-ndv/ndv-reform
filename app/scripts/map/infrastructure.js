const infrastructure = [
	{coords: [55.800577, 37.715020], icon: {src: '/pic/design/map-abc.png', w: 35, h: 45}},
	{coords: [55.799223, 37.720427], properties: {title: 'Some title'}, icon: {src: '/pic/design/map-transport.png', w: 35, h: 45}},
	{coords: [55.801495, 37.718024], properties: {title: 'Some dasdas title'}, icon: {src: '/pic/design/map-market.png', w: 35, h: 45}},
	{coords: [55.799803, 37.722916], properties: {title: 'Some'}, icon: {src: '/pic/design/map-metro.png', w: 35, h: 45}},
	{coords: [55.799513, 37.717766], properties: {title: 'Some title'}, icon: {src: '/pic/design/map-pocket.png', w: 35, h: 45}}
];

export default infrastructure;
