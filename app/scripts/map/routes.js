export default [
	{
		coords: [{lat: 55.796860, lng: 37.717730}, {lat: 55.799821, lng: 37.716979}, {lat: 55.800911, lng: 37.723395}],
		strokeColor: '#FFF'
	},
	{
		coords: [{lat: 55.798721, lng: 37.728920}, {lat: 55.801598, lng: 37.727332}, {lat: 55.800631, lng: 37.720895}],
		strokeColor: '#e535d6'
	}
];
