/* globals $ */
import scrollbar from 'perfect-scrollbar';
import navigation from './navigation';
import routes from './routes';
import './libs/fancybox';

const view = routes.getView();
if (view) {
	view();
}

require.ensure('./application', () => {
	const application = require('./application');
	application.initialize();
	$('.request').on('click', function() {
		application.toggle();
	});
}, 'application');

navigation.initialize();

$('.scrollbar.scrollbar-arrow').each(function() {
	scrollbar.initialize(this, {suppressScrollX: true, maxScrollbarLength: 15});
});

$('.fancybox').fancybox();

/*
	Fancybox on about page
 */
$('a[rel="fancy-group"]').fancybox();
$('.fancy-group').on('click', function(event) {
	event.preventDefault();
	$('a[rel="fancy-group"]').eq(0).click();
});

$('.video-link').fancybox();
