/* globals $ */
import {transitionShow, transitionHide} from './utils';

let container;

export default {
	initialize: function() {
		container = $('.application-form');

		container.find('.close').on('click', this.toggle.bind(this));
		$(document).on('keyup', (event) => {
			if (event.keyCode === 27 && container.hasClass('is-open')) {
				this.toggle();
			}
		});
	},

	toggle: function() {
		const isOpen = container.hasClass('is-open');
		if (isOpen) {
			transitionHide(container[0], 'is-open', 'is-animate');
		} else {
			transitionShow(container[0], 'is-open', 'is-animate');
		}
	}
};
